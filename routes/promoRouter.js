const express = require('express')

const authenticate = require('../authenticate')
const cors = require('./cors')
const Promotions = require('../models/promotions')

const promoRouter = express.Router()

promoRouter.route('/')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.cors, (req, res, next) => Promotions.find({})
    .then(promotions => res.status(200).json({
      message: 'Promotions fetched successfully',
      data: promotions
    }), 
    err => next(err))
    .catch(err => next(err))
  )
  .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Promotions.create(req.body)
    .then(promotion => res.status(200).json({
      message: 'Promotion added successfully',
      date: promotion
    }),
    err => next(err))
    .catch(err => next(err))
  )
  .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    const err = new Error('not supported')
    err.status = 403
    next(err)
  })
  .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Promotions.deleteMany({})
    .then(resp => res.status(200).json({
      message: 'Promotions deleted successfully'
    }),
    err => next(err))
    .catch(err => next(err))
  )

promoRouter.route('/:promoId')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.cors, (req, res, next) => Promotions.findById(req.params.promoId)
      .then(promotion => res.status(200).json({
        message: 'Promotion fetched successfully',
        data: promotion
      }),
      err => next(err))
      .catch(err => next(err))
  )
  .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    const err = new Error('not supported')
    err.status = 403
    next(err)
  })
  .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Promotions.findByIdAndUpdate(req.params.promoId,{
    $set: req.body
  },{
    new: true,
    useFindAndModify: false
  })
    .then(promotion => res.status(200).json({
      message: 'Promotion updated successfully',
      data: promotion
    }),
    err => next(err))
    .catch(err => next(err))
  )
  .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Promotions.findByIdAndDelete(req.params.promoId)
    .then(resp => res.status(200).json({
      message: 'Promotion deleted successfully'
    }),
    err => next(err))
    .catch(err => next(err))
  )

module.exports = promoRouter