const express = require('express')

const authenticate = require('../authenticate')
const cors = require('./cors')
const Dishes = require('../models/dishes')
const Favorites = require('../models/favorites')

const favoritesRouter = express.Router()

favoritesRouter.route('/')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => Favorites.findOne({ user: req.user._id }).populate('user dishes')
    .then(favorite => res.status(200).json({
      message: 'Favorites fetched successfully',
      data: favorite
    }),
    err => next(err))
    .catch(err => next(err))
  )
  .post(cors.corsWithOptions, authenticate.verifyUser, async (req, res, next) => {
    let invalidIds = []
    let errMess = 'dish(s) '
    for(let i = 0; i < req.body.length; i++) await Dishes.findById(req.body[i]._id)
      .then(dish => {
        if(!dish) {
          invalidIds.push(req.body[i]._id)
          errMess += req.body[i]._id + ' '
        }
      }, err => next(err))
      .catch(err => next(err))
    
    if(invalidIds.length) {
      const err = new Error(errMess + 'not found')
      err.status = 404
      next(err)
    } else Favorites.findOne({ user: req.user._id })
      .then(favorite => {
        if(favorite) {
          for(let i = 0; i < req.body.length; i++) 
            if(favorite.dishes.indexOf(req.body[i]._id) === -1) favorite.dishes.push(req.body[i]._id)

          favorite.save()
            .then(favorite => res.status(200).json({
              message: 'Dishes added to your favorites succesfully',
              data: favorite
            }),
            err => next(err))
            .catch(err => next(err))
        } else {
          const newFavorite = new Favorites({
            user: req.user._id
          })
          for(let i = 0; i < req.body.length; i++) newFavorite.dishes.push(req.body[i]._id)
          
          newFavorite.save()
            .then(favorite => res.status(200).json({
              message: 'Dishes added to your favorites succesfully',
              data: favorite
            }),
            err => next(err))
            .catch(err => next(err))
        }
      }, err => next(err))
      .catch(err => next(err))
  })
  .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => Favorites.findOneAndDelete({ user: req.user._id })
    .then(resp => res.status(200).json({
      message: 'Favorites removed successfully'
    }),
    err => next(err))
    .catch(err => next(err))
  )

favoritesRouter.route('/:dishId')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => Dishes.findById(req.params.dishId)
    .then(dish => {
      if(dish) Favorites.findOne({ user: req.user._id })
        .then(favorite => {
          if(favorite && favorite.dishes.indexOf(req.params.dishId) !== -1) {
            const err = new Error('already favorite')
            err.status = 403
            next(err)
          } else if(favorite && favorite.dishes.indexOf(req.params.dishId) === -1) {
            favorite.dishes.push(req.params.dishId)
            favorite.save()
              .then(favorite => res.status(200).json({
                message: 'Dish added to your favorites successfully',
                data: favorite
              }),
              err => next(err))
              .catch(err => next(err))
          } else if(!favorite) {
            const newFavorite = new Favorites({
              user: req.user._id
            }) 
            newFavorite.dishes.push(req.params.dishId)
            newFavorite.save()
              .then(favorite => res.status(200).json({
                message: 'Dish added to your favorites succesfully',
                data: favorite
              }),
              err => next(err))
              .catch(err => next(err))
          }
        }, err => next(err))
        .catch(err => next(err))
      else {
        const err = new Error('dish ' + req.params.dishId + ' doesn\'t exist')
        err.status = 404
        next(err)
      }
    }, err => next(err))
    .catch(err => next(err))
  )
  .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => Dishes.findById(req.params.dishId)
    .then(dish => {
      if(dish) Favorites.findOne({ user: req.user._id })
        .then(favorite => {
          if(favorite) {
            const index = favorite.dishes.indexOf(req.params.dishId)
            if(index !== -1) {
              favorite.dishes.splice(index, 1)
              
              if(!favorite.dishes.length) favorite.remove()
                .then(resp => res.status(200).json({
                  message: 'All dishes removed from favorites successfully'
                }),
                err => next(err))
                .catch(err => next(err))
              else favorite.save()
                .then(favorite => res.status(200).json({
                  message: 'Dish removed from favorites successfully',
                  data: favorite
                }),
                err => next(err))
                .catch(err => next(err))
            } else {
              const err = new Error('already unfavorite')
              err.status = 403
              next(err)
            }
          } else {
            const err = new Error('favorites doesn\'t exist for this user')
            err.status = 404
            next(err)
          }
        }, err => next(err))
        .catch(err => next(err))
      else {
        const err = new Error('dish ' + req.params.dishId + ' doesn\'t exist')
        err.status = 404
        next(err)
      }
    })
  )

module.exports = favoritesRouter