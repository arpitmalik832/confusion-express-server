const express = require('express')

const authenticate = require('../authenticate')
const cors = require('./cors')
const Dishes = require('../models/dishes')

const dishRouter = express.Router()

dishRouter.route('/')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.cors, (req, res, next) => Dishes.find({})
    .populate('comments.author')
    .then(dishes => res.status(200).json({
      message: 'Dishes fetched successfully',
      data: dishes
    }),
    err => next(err))
    .catch(err => next(err))
  )
  .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Dishes.create(req.body)
    .then(dish => res.status(200).json({
      message: 'Dish added successfully',
      data: dish
    }),
    err => next(err))
    .catch(err => next(err))
  )
  .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    const err = new Error('not supported')
    err.status = 403
    next(err)
  })
  .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Dishes.deleteMany({})
    .then(resp => res.status(200).json({
      message: 'Dishes deleted successfully'
    }),
    err => next(err))
    .catch(err => next(err))
  )

dishRouter.route('/:dishId')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.cors, (req, res, next) => Dishes.findById(req.params.dishId)
    .populate('comments.author')
    .then(dish => {
      if(dish) res.status(200).json({
          message: 'Dish fetched successfully',
          data: dish
        })
      else {
        const err = new Error('dish ' + req.params.dishId + ' doesn\'t exist')
        err.status = 404
        next(err)
      }
    }, err => next(err))
    .catch(err => next(err))
  )
  .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    const err = new Error('not supported')
    err.status = 403
    next(err)
  })
  .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Dishes.findByIdAndUpdate(req.params.dishId, {
    $set: req.body
  },{
    new: true,
    useFindAndModify: false
  })
    .then(dish => Dishes.findById(dish._id)
      .populate('comments.author')
      .then(dish => res.status(200).json({
        message: 'Dish updated successfully',
        data: dish
      }),
      err => next(err))
      .catch(err => next(err)),
    err => next(err))
    .catch(err => next(err))
  )
  .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Dishes.findByIdAndDelete(req.params.dishId)
    .then(resp => res.status(200).json({
      message: 'Dish deleted successfully'
    }),
    err => next(err))
    .catch(err => next(err))
  )

dishRouter.route('/:dishId/comments')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.cors, (req, res, next) => Dishes.findById(req.params.dishId)
    .populate('comments.author')
    .then(dish => {
      if(dish) res.status(200).json({
        message: 'Comments Fetched successfully',
        data: dish.comments
      })
      else {
        const err = new Error('dish ' + req.params.dishId + ' doesn\'t exist')
        err.status = 404
        next(err)
      }
    }, err => next(err))
    .catch(err => next(err))
  )
  .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => Dishes.findById(req.params.dishId)
    .then(dish => {
      if(dish) {
        req.body.author = req.user._id
        dish.comments.push(req.body)
        dish.save()
          .then(dish => Dishes.findById(dish._id)
            .populate('comments.author')
            .then(dish => res.status(200).json({
              message: 'Comment added successfully',
              data: dish.comments
            }),
            err => next(err))
            .catch(err => next(err)),
          err => next(err))
          .catch(err => next(err))
      } else {
        const err = new Error('dish ' + req.params.dishId + ' doesn\'t exist')
        err.status = 404
        next(err)
      }
    }, err => next(err))
    .catch(err => next(err))
  )
  .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    const err = new Error('not supported')
    err.status = 403
    next(err)
  })
  .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Dishes.findById(req.params.dishId)
    .then(dish => {
      if(dish) {
        for(var i = (dish.comments.length - 1); i>=0; i--) {
          dish.comments.id(dish.comments[i]._id).remove()
        }
        dish.save()
          .then(dish => res.status(200).json({
            message: 'Comments deleted successfully'
          }),
          err => next(err))
          .catch(err => next(err))
      } else {
        const err = new Error('dish ' + req.params.dishId + ' doesn\'t exist')
        err.status = 404
        next(err)
      }
    },
    err => next(err))
    .catch(err => next(err))  
  )

dishRouter.route('/:dishId/comments/:commentId')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.cors, (req, res, next) => Dishes.findById(req.params.dishId)
    .populate('comments.author')
    .then(dish => {
      if(dish && dish.comments.id(req.params.commentId)) 
        res.status(200).json({
          message: 'Comment Fetched successfully',
          data: dish.comments.id(req.params.commentId)
        })
      else if(dish == null) {
        const err = new Error('dish ' + req.params.dishId + ' doesn\'t exist')
        err.status = 404
        next(err)
      } else {
        const err = new Error('comment ' + req.params.commentId + ' doesn\'t exist')
        err.status = 404
        next(err)
      }
    }, err => next(err))
    .catch(err => next(err))
  )
  .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    const err = new Error('not supported')
    err.status = 403
    next(err)
  })
  .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => Dishes.findById(req.params.dishId)
    .then(dish => {
      if(dish && dish.comments.id(req.params.commentId) && req.user._id.toString() === dish.comments.id(req.params.commentId).author.toString()) {
        if (req.body.rating) {
          dish.comments.id(req.params.commentId).rating = req.body.rating
        }
        if(req.body.comment) {
          dish.comments.id(req.params.commentId).comment = req.body.comment
        }
        dish.save()
          .then(dish => {
            Dishes.findById(dish._id).populate('comments.author')
              .then(dish => res.status(200).json({
                message: 'Comment updated successfully',
                data: dish.comments.id(req.params.commentId)
              }),
              err => next(err))
              .catch(err => next(err))
          }, err => next(err))
          .catch(err => next(err))
      } else if(!dish) {
        const err = new Error('dish ' + req.params.dishId + ' doesn\'t exist')
        err.status = 404
        next(err)
      } else if(!dish.comments.id(req.params.commentId)) {
        const err = new Error('comment ' + req.params.commentId + ' doesn\'t exist')
        err.status = 404
        next(err)
      } else {
        const err = new Error('unauthorized')
        err.status = 401
        next(err)
      }
    }, err => next(err))
    .catch(err => next(err))
  )
  .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => Dishes.findById(req.params.dishId)
    .then(dish => {
      if(dish && dish.comments.id(req.params.commentId) && (req.user._id.toString() === dish.comments.id(req.params.commentId).author.toString() || req.user.admin)) {
        dish.comments.id(req.params.commentId).remove()
        dish.save()
          .then(dish => Dishes.findById(dish._id)
            .populate('comments.author')
            .then(dish => res.status(200).json({
              message: 'Comment deleted successfully'
            }),
            err => next(err))
            .catch(err => next(err)),
          err => next(err))
          .catch(err => next(err))
      } else if(!dish) {
        const err = new Error('dish ' + req.params.dishId + ' doesn\'t exist')
        err.status = 404
        next(err)
      } else if(!dish.comments.id(req.params.commentId)) {
        const err = new Error('comment ' + req.params.commentId + ' doesn\'t exist')
        err.status = 404
        next(err)
      } else {
        const err = new Error('unauthorized')
        err.status = 401
        next(err)
      }
    }, err => next(err))
    .catch(err => next(err))
  )

module.exports = dishRouter