const express = require('express')

const authenticate = require('../authenticate')
const cors = require('./cors')
const Leaders = require('../models/leaders')

const leaderRouter = express.Router()

leaderRouter.route('/')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.cors, (req, res, next) => Leaders.find({})
    .then(leaders => res.status(200).json({
      message: 'Leaders fetched successfully',
      data: leaders
    }),
    err => next(err))
    .catch(err => next(err))
  )
  .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Leaders.create(req.body)
    .then(leader => res.status(200).json({
      message: 'Leader created succesfully',
      data: leader
    }),
    err => next(err))
    .catch(err => next(err))
  )
  .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
      const err = new Error('not supported')
      err.status = 403
      next(err)
  })
  .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Leaders.deleteMany({})
    .then(resp => res.status(200).json({
      message: 'Leaders deleted successfully'
    }),
    err => next(err))
    .catch(err => next(err))
  )

leaderRouter.route('/:leaderId')
  .options(cors.corsWithOptions, (req, res) => res.sendStatus(200))
  .get(cors.cors, (req, res, next) => Leaders.findById(req.params.leaderId)
    .then(leader => res.status(200).json({
      message: 'Leader fetched successfully',
      data: leader
    }),
    err => next(err))
    .catch(err => next(err))
  )
  .post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => {
    const err = new Error('not supported')
    err.status = 403
    next(err)
  })
  .put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Leaders.findByIdAndUpdate(req.params.leaderId,{
    $set: req.body
  },{
    new: true,
    useFindAndModify: false
  })
    .then(leader => res.status(200).json({
      message: 'Leader updated successfully',
      data: leader
    }),
    err => next(err))
    .catch(err => next(err))
  )
  .delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Leaders.findByIdAndDelete(req.params.leaderId)
    .then(resp => res.status(200).json({
      message: 'Leader deleted successfully'
    }),
    err => next(err))
    .catch(err => next(err))
  )

module.exports = leaderRouter