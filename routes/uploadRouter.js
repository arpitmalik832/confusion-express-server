const express = require('express')
const multer = require('multer')

const authenticate = require('../authenticate')
const cors = require('./cors')

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/images')
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname)
  }
})

const imageFileFilter = (req, file, cb) => {
  if(!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
    return cb(new Error('you can upload only image files', false))
  } else {
    cb(null, true)
  }
}

const upload = multer({
  storage,
  fileFilter: imageFileFilter
})

const uploadRouter = express.Router()

uploadRouter.post('/', cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, upload.single('imageFile'), (req, res, next) => {
  if(req.file) res.status(200).json({
    message: 'Image uploaded successfully',
    data: req.file
  })
  else {
    const err = new Error('image not found')
    err.status = 404
    next(err)
  }
})

module.exports = uploadRouter