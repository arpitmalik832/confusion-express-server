const passport = require('passport')
const express = require('express')

const authenticate = require('../authenticate')
const cors = require('./cors')
const Users = require('../models/users')

const userRouter = express.Router()

userRouter.get('/', cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req, res, next) => Users.find({})
  .then(users => res.status(200).json({
    message: 'Users fetched successfully',
    data: users
  }),
  err => next(err))
  .catch(err => next(err))
)

userRouter.post('/signup', cors.corsWithOptions, (req, res, next) => Users.register(new Users({
    username: req.body.username
  }), req.body.password, (err, user) => {
    if(err) {
      err.status = 500
      next(err)
    } else {
      if(req.body.firstname) user.firstname = req.body.firstname
      if(req.body.lastname) user.lastname = req.body.lastname
      user.save((err, user) => {
        if(err) {
          err.status = 500
          next(err)
          return
        }
        passport.authenticate('local')(req, res, () => res.status(200).json({
          success: true,
          status: 'Registration Successful'
        }))
      })
    }
  })
)

userRouter.post('/login', cors.corsWithOptions, passport.authenticate('local'), (req, res, next) => {
  const token = authenticate.getToken({
    _id: req.user._id
  })
  res.status(200).json({
    success: true,
    status: 'You are successfully logged in',
    token
  })
})

//not working
userRouter.get('/logout', cors.corsWithOptions, (req, res, next) => {
  if (req.user) {
    req.session.destroy()
    res.clearCookie('session-id')
    res.redirect('/')
  } else {
    const err = new Error('not logged in')
    err.status = 403
    next(err)
  }
})

userRouter.get('/facebook/token', passport.authenticate('facebook-token'), (req, res, next) => {
  if(req.user) {
    const token = authenticate.getToken({
      _id: req.user._id
    })
    res.status(200).json({
      success: true,
      status: 'You are successfully logged in',
      token
    })
  }
})

module.exports = userRouter