const passport = require('passport')
const jwt = require('jsonwebtoken')
const LocalStategy = require('passport-local').Strategy
const JwtStratergy = require('passport-jwt').Strategy
const FacebookTokenStratergy = require('passport-facebook-token')
const ExtractJwt = require('passport-jwt').ExtractJwt
const Users = require('./models/users')

const config = require('./config')

exports.local = passport.use(
  new LocalStategy(
    Users.authenticate()
  )
)

passport.serializeUser(Users.serializeUser())
passport.deserializeUser(Users.deserializeUser())

exports.getToken = function (user) {
  return jwt.sign(user, config.secretKey, {
    expiresIn: 3600
  })
}

const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
opts.secretOrKey = config.secretKey

exports.jwtPassport = passport.use(
  new JwtStratergy(
    opts,
    (jwt_payload, done) => {
      Users.findOne({_id: jwt_payload._id}, (err, user) => {
        if(err) return done(err, false)
        else if(user) return done(null, user)
        else return done(null, false)
      })
    }
  )
)

exports.verifyUser = passport.authenticate('jwt', { session: false })

exports.verifyAdmin = (req, res, next) => {
  if(req.user.admin) next()
  else {
    const err = new Error('unauthorized')
    err.status = 401
    next(err)
  }
}

exports.facebookPassport = passport.use(new FacebookTokenStratergy({
  clientID: config.facebook.clientId,
  clientSecret: config.facebook.clientSecret
}, (accessToken, refreshToken, profile, done) => {
  Users.findOne({facebookId: profile.id}, (err, user) => {
    if(err) return done(err, false)
    else if(!err && user) return done(null, user)
    else {
      user = new Users({
        username: profile.displayName
      })
      user.facebookId = profile.id
      user.firstname = profile.name.givenName
      user.lastname = profile.name.familyName
      user.save((err, user) => {
        if(err) return done(err, false)
        else return done(null, user)
      })
    }
  })
}))