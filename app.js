const path = require('path')
const createError = require('http-errors')
const passport = require('passport')
const authenticate = require('./authenticate')
const config = require('./config')
const logger = require('morgan')
const express = require('express')
const mongoose = require('mongoose')

const indexRouter = require('./routes/indexRouter')
const userRouter = require('./routes/userRouter')
const dishRouter = require('./routes/dishRouter')
const leaderRouter = require('./routes/leaderRouter')
const promoRouter = require('./routes/promoRouter')
const uploadRouter = require('./routes/uploadRouter')
const favoriteRouter = require('./routes/favoriteRouter')

const connect = mongoose.connect(config.mongoUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
})

connect.then(db => {
  console.log('Connected Correctly to server')
})
.catch(err => console.log(err))

const app = express()

app.all('*', (req, res, next) => {
  if(req.secure) {
    return next()
  } else {
    res.redirect(307, 'https://' + req.hostname + ':' + app.get('secPort') + req.url)
  }
})

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use(passport.initialize())

app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/users', userRouter)
app.use('/dishes', dishRouter)
app.use('/leaders', leaderRouter)
app.use('/promotions', promoRouter)
app.use('/imageUpload', uploadRouter)
app.use('/favorites', favoriteRouter)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404))
})

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app